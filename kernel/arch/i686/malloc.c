#include<stddef.h>
#include<stdbool.h>
#include<stdint.h>
#include<kernel/system.h>

/***********************************************************************
 * Memory allocation routine.
 * Uses a linked list based table to do accounting for allocated
 * segments.
 *
 * Does not account for paging. For use with a flat memory model kernel
 ***********************************************************************/

struct tableNode_s
{
	size_t sizeBytes;
	size_t maxSize;
	unsigned char* data;
	struct tableNode* next;
} tableNode_default = {0,0,NULL,NULL};

typedef struct tableNode_s tableNode;

unsigned char* baseAddr = (unsigned char*)0x01000000;
unsigned char* maxAddr = (unsigned char*)0xBFFFFFFF;
unsigned char* curAddr = (unsigned char*)0x01000000;
const tableNode* MALLOCHEAD;

unsigned char* allocateNewSegment(size_t sizeBytes)
{
	curAddr = curAddr + sizeBytes;
	if (curAddr >= maxAddr)
		return NULL; //OOM
	return curAddr - sizeBytes;;
}

tableNode* allocate(size_t segmentSize, tableNode* cur)
{
	if (cur->sizeBytes == 0 && cur->data == NULL) //Brand new node
	{
		cur->data = allocateNewSegment(segmentSize);
		cur->sizeBytes = segmentSize;
		cur->maxSize = segmentSize;
		return cur;
	}
	else if (cur->maxSize <= segmentSize) //Reuse a segment even if its too big. First fit
	{
		cur->sizeBytes = segmentSize;
		return cur;
	}
	//to make this a bit more efficient maybe have it split the segment and create a new entry in the table that is (Oldsize - smallernewsize) bytes
	else if (cur->next == NULL)
	{
		tableNode new;
		cur->next = &new;
		return allocate(segmentSize, cur->next);
	}
	else
		return allocate(segmentSize, cur->next);
}

int deleteSegment(unsigned char* segment, tableNode* cur)
{
	if (cur->data == segment)
		cur->sizeBytes = 0;
	else if (cur->next != NULL)
		deleteSegment(segment,cur->next);
	else
		return NULL;
	return 0;	
}

void* malloc(size_t size)
{
	tableNode* segment = allocate(size, MALLOCHEAD);
	return segment->data;
}

int free(unsigned char* segment)
{
	return deleteSegment(segment, MALLOCHEAD);
}

int c = 0;
tableNode* dumpCur = MALLOCHEAD;

int dumpPage()
{
  tcputs("SEGMENT ", COLOR_RED);
  tcputs(itoa(c), COLOR_RED);
  tcputs("Size: ", COLOR_WHITE);
  tputs(itoa(dumpCur->sizeBytes));
  tputs("\n");
  tcputs("MaxSize: ", COLOR_WHITE);
  tputs(itoa(dumpCur->maxSize));
  tputs("\n");
  tcputs("dataPointer: ", COLOR_WHITE);
  tputs(itoa(dumpCur->data));
  tputs("\n");
  c++;
  if (dumpCur->next == NULL)
  {
    dumpCur = MALLOCHEAD;
    c = 0;
  }
  else
    dumpCur = dumpCur->next;
}

int memDump()
{
  tableNode* cur = MALLOCHEAD;
  while(cur != NULL)
  {
    dumpPage(cur, c);
    cur = cur->next;
    c++;
  }
  c = 0;
}
